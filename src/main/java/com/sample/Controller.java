package com.sample;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

	@RequestMapping("/greet")
	public String greeting() {
		return "hello there! ";
	}
	
	@RequestMapping("/greet/{id}")
	public String hello(@PathVariable("id") String id) {
		return "Hello "+id;
	}

}